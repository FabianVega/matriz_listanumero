/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.ListaNumeros;

/**
 * Modelamiento de una matriz usando el concepto de vector de vectores
 *
 * @author madarme
 */
public class Matriz_Numeros {

    private ListaNumeros[] filas;

    public Matriz_Numeros() {
    }

    /**
     * Puedo utilizarlo para matrices dispersa
     *
     * @param cantFilas
     */
    public Matriz_Numeros(int cantFilas) {
        if (cantFilas <= 0) {
            throw new RuntimeException("No se puede crear la matriz , sus cantidad de filas debe ser mayor a 0");
        }
        this.filas = new ListaNumeros[cantFilas];
    }

    /**
     * Creación de matrices cuadradas o bien rectangulares
     *
     * @param cantFilas
     * @param cantColumnas
     */
    public Matriz_Numeros(int cantFilas, int cantColumnas) {
        if (cantFilas <= 0 || cantColumnas <= 0) {
            throw new RuntimeException("No se puede crear la matriz , sus cantidad de filas o columnas debe ser mayor a 0");
        }
        this.filas = new ListaNumeros[cantFilas];
        //Creando columnas:
        for (int i = 0; i < cantFilas; i++) {
            this.filas[i] = new ListaNumeros(cantColumnas);
        }

    }

    public void leerMatriz(String cadena) {

        if (cadena.isEmpty()) {
            throw new RuntimeException("Es imposible crear la matriz, no hay datos");
        }
        String filasD[] = cadena.split(";");

        this.filas = new ListaNumeros[filasD.length];

        for (int i = 0; i < filasD.length; i++) {
            String columnasDatos[] = filasD[i].split(",");
            ListaNumeros columna = new ListaNumeros(columnasDatos.length);

            this.pasarDatos_Columna(columna, columnasDatos);

            this.adicionarVector(i, columna);

        }
    }

    private void pasarDatos_Columna(ListaNumeros columna, String datos[]) {
        for (int i = 0; i < datos.length; i++) {
            columna.adicionar(i, Float.parseFloat(datos[i]));

        }
    }

    public ListaNumeros[] getFilas() {
        return filas;
    }

    public void setFilas(ListaNumeros[] filas) {
        this.filas = filas;
    }

    private void validar(int i) {
        if (i < 0 || i >= this.filas.length) {
            throw new RuntimeException("Índice fuera de rango para una fila:" + i);
        }
    }

    public void adicionarVector(int i, ListaNumeros listaNueva) {
        this.validar(i);
        this.filas[i] = listaNueva;
    }

    public ListaNumeros getElementoVector(int i) {

        this.validar(i);
        return this.filas[i];
    }

    /**
     * Esté mètodo funciona SI Y SOLO SI LA MATRIZ ESTÁ CREADA CON FILAS Y
     * COLUMNAS
     *
     * @param i índice de la fila
     * @param j índice de la columna
     * @param nuevoDato dato a ingresar en i,j
     */
    public void setElemento(int i, int j, float nuevoDato) {

        this.filas[i].adicionar(i, nuevoDato);

    }

    @Override
    public String toString() {
        String msg = "";

        for (ListaNumeros myLista : this.filas) {
            msg += myLista.toString() + "\n";
        }
        return msg;
    }

    public int length_filas() {
        return this.filas.length;
    }

    public int length_columnas() {
        return this.filas[0].length();

    }

    /**
     *
     * 🅼🅴🆃🅾🅳🅾🆂 🅳🅴 🅻🅰 🅰🅲🆃🅸🆅🅸🅳🅰🅳
     *
     *
     *
     */
    /**
     *
     * @param m1
     * @return
     */
    /**
     * Metodo para comparar Si son de la misma taxonomia
     *
     * @param m1 matriz con la que se quiere comparar
     * @return true si son iguales, false si son diferentes
     */
    public boolean esValida(Matriz_Numeros m1) {
        boolean valid = true;
        if (m1.length_filas() == this.length_filas()) {

            for (int i = 0; valid && i < m1.length_filas(); i++) {
                ListaNumeros s[] = m1.getFilas();
                ListaNumeros s2[] = this.getFilas();
                if (s[i].length() != s2[i].length()) {
                    valid = false;

                } else {
                    valid = true;
                }
            }

        } else {
            valid = false;
        }

        return valid;
    }

    /**
     * Metodo para sumar 2 matrices de la misma taxonomia
     *
     * @param m1
     * @return Matriz resultante con la misma taxonomia 
     */
    public Matriz_Numeros getSuma(Matriz_Numeros m1) {
        Matriz_Numeros mRes = null;

        if (this.esValida(m1)) {
            mRes = new Matriz_Numeros(m1.length_filas());

            for (int i = 0; i < m1.length_filas(); i++) {

                ListaNumeros sRes = new ListaNumeros(m1.filas[i].length());
                for (int j = 0; j < m1.filas[i].length(); j++) {
                    float valor = m1.filas[i].getElemento(j);
                    float valor2 = this.filas[i].getElemento(j);

                    float suma = valor + valor2;
                    sRes.adicionar(j, suma);
                }
                mRes.adicionarVector(i, sRes);
            }
        }

        return mRes;
    }

    /**
     * Metodo para restar 2 matrices con misma taxonomia
     *
     * @param m1
     * @return Matriz resultante con la misma taxonomia 
     */
    public Matriz_Numeros getResta(Matriz_Numeros m1) {
        Matriz_Numeros mRes = null;
        if (this.esValida(m1)) {
            mRes = new Matriz_Numeros(m1.length_filas());

            for (int i = 0; i < m1.length_filas(); i++) {

                ListaNumeros sRes = new ListaNumeros(m1.filas[i].length());
                for (int j = 0; j < m1.filas[i].length(); j++) {
                    float valor = m1.filas[i].getElemento(j);
                    float valor2 = this.filas[i].getElemento(j);

                    float suma = valor - valor2;
                    sRes.adicionar(j, suma);
                }
                mRes.adicionarVector(i, sRes);
            }
        }
        return mRes;
    }

    /**
     * Para multiplicar 2 matrices es necesario cumplir con la siguiente condicion:
     * Sea A una matriz de orden (m x n), y B una matriz de orde (n x r),
     * el producto dara una matriz C = A . B de orden (m x r)
     *
     * @param m1 Matriz con la que se desea multiplicar
     * @return Matriz resultante de diferente dimension
     * 
     */
    public Matriz_Numeros getMultiplicacion(Matriz_Numeros m1) {
        Matriz_Numeros mRes = null;

        float[][] resultado = new float[this.length_filas()][m1.length_columnas()];
        if (this.validarMultiplicacion(m1)) {
            mRes = new Matriz_Numeros(this.length_filas(), m1.length_columnas());
            for (int i = 0; i < this.length_filas(); i++) {
                for (int j = 0; j < m1.length_columnas(); j++) {
                    for (int k = 0; k < this.length_columnas(); k++) {
                        
                        resultado[i][j] += this.filas[i].getElemento(k) * m1.filas[k].getElemento(j);
                        System.out.println(resultado[j]);
                        System.out.println(j+"------");
                        mRes.filas[i].adicionar(j, resultado[i][j]);
                        
                    }
                    
                }
                
            }
            
        }
        return mRes;
    }
    /**
     *  La transpuesta de una matriz consiste en intercambiar las filas por las columnas
     *  Si A es una matriz (m x n) sera una matriz (n x m)
     *
     * @return  Retorna la matriz transpuesta
     */
    public Matriz_Numeros getTranspuesta() {
        Matriz_Numeros mTrans = null;
        if (this.validaMatrizCuadrada()) {
            mTrans = new Matriz_Numeros(this.length_filas());
            for (int i = 0; i < this.length_filas(); i++) {

                ListaNumeros sTrans = new ListaNumeros(this.filas[i].length());

                for (int j = 0; j < this.filas[i].length(); j++) {

                    float valor = this.filas[j].getElemento(i);

                    sTrans.adicionar(j, valor);

                }
                mTrans.adicionarVector(i, sTrans);

            }

        } else if (this.validarMatrizRectangular()) {
            mTrans = new Matriz_Numeros(this.filas[0].length(), this.length_filas());

            for (int i = 0; i < this.length_filas(); i++) {

                for (int j = 0; j < this.filas[i].length(); j++) {

                    float valor = this.filas[i].getElemento(j);

                    System.out.println(valor);
                    mTrans.filas[j].adicionar(i, valor);

                }

            }
        }
        return mTrans;
    }
    
    /**
     * MEtodo para validar si la matriz es cuadrada, es decir una matriz (mxm)
     * 
     * @return Retorna un boolean 
     */
    
    public boolean validaMatrizCuadrada() {

        boolean validar = true;
        for (int i = 0; validar && i < this.length_filas(); i++) {
            if (this.filas[i].length() == this.length_filas()) {
                System.out.println("es cuadrada");
                validar = true;

            } else {
                validar = false;
                System.out.println("eeeeeeeeeeeeeeeeeeee");
            }

        }
        return validar;

    }

    public boolean validarMatrizRectangular() {
        ListaNumeros s[] = this.getFilas();
        int contadorf = this.length_filas();
        int contadorc = s[0].length();
        boolean validarc = true;
        boolean validar = false;
        for (int i = 0; validarc && i < contadorf; i++) {
            if (s[i].length() == contadorc) {
                validarc = true;
                System.out.println("aaaaa");
            }
        }
        if (validarc && (contadorf < contadorc || contadorf > contadorc)) {
            validar = true;
            System.out.println("es rectangular");

        }
        return validar;

    }

    public boolean validarMultiplicacion(Matriz_Numeros m1) {

        boolean validar = false;
        boolean validarC = false;
        int contadorc = this.filas[0].length();
        int contadorc2 = m1.filas[0].length();

        for (int i = 0; i < this.filas.length; i++) {
            if ((this.filas[i].length() == contadorc) && (m1.filas[i].length() == contadorc2)) {
                validarC = true;
                System.out.println("arre");
            }
        }
        if (validarC) {
            if (this.length_filas() == contadorc2) {
                validar = true;
                System.out.println("chinga");
            }
        } else {
            validar = false;

        }
        return validar;

    }

}
